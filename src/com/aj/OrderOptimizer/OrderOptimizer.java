package com.aj.OrderOptimizer;

import java.awt.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.text.html.parser.Entity;

public class OrderOptimizer {

	public static void main(String[] args) {
		BufferedReader br = null;
		String inputFile = args[0];
		String rowItems = null;
		ConcurrentHashMap<String, String> hotelHits = new ConcurrentHashMap<String, String>();
		Map<Float, String> finalTree = new TreeMap<Float, String>();
		int itemNum = args.length - 1;
		try {
			br = new BufferedReader(new FileReader(inputFile));
			while ((rowItems = br.readLine()) != null) {
				String[] Items = rowItems.split(",");
				for (int i = 1; i <= itemNum; i++) {
					if (Items.length == 3) {
						if (i == 1) {
							if (args[i].trim().equals(Items[2].trim())) {
								hotelHits.put(Items[0].toString(),
										Items[1].toString() + ',' + '1');
								
							}
						} else {
							if (args[i].trim().equals(Items[2].trim())) {
								if(hotelHits.containsKey(Items[0])){
									String value = Float.toString((Float.parseFloat(hotelHits.get(Items[0]).split(",")[0])+Float.parseFloat(Items[1]))) + ',' + (Integer.parseInt(hotelHits.get(Items[0]).split(",")[1])+1);
									hotelHits.put(Items[0].toString(),value);
								}
							} 
						}
					}
					else {
						for(int j=2; j<Items.length; j++){
							if(i==1){
								if(args[i].trim().equals(Items[j].trim())){
									hotelHits.put(Items[0].toString(),
											Items[1].toString() + ',' + '1');
									
								}
							} else {
								if(args[i].trim().equals(Items[j].trim())){
									if(hotelHits.containsKey(Items[0])){
										String value = Float.toString((Float.parseFloat(hotelHits.get(Items[0]).split(",")[0])+Float.parseFloat(Items[1]))) + ',' + (Integer.parseInt(hotelHits.get(Items[0]).split(",")[1])+1);
										hotelHits.put(Items[0].toString(),value);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(Map.Entry<String, String> entry : hotelHits.entrySet()){
			if(Integer.parseInt(entry.getValue().split(",")[1].trim())<itemNum)
				hotelHits.remove(entry.getKey());
			else
				finalTree.put(Float.parseFloat(entry.getValue().split(",")[0].trim()), entry.getKey());
			}
		
		if(hotelHits.size()==0)
			System.out.println("nil");
		int rank = 1;
		for(Map.Entry<Float, String> entry : finalTree.entrySet()){
			if(rank==1)
				System.out.println(entry.getValue()+", "+entry.getKey());
			rank++;
			}
	}
}