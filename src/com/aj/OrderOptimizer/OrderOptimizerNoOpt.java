package com.aj.OrderOptimizer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class OrderOptimizerNoOpt {

	public static void main(String[] args) {
		BufferedReader br = null;
		String inputFile = args[0];
		String rowItems = null;
		ConcurrentHashMap<String, String> hotelHits = new ConcurrentHashMap<String, String>();
		int itemNum = args.length - 1;
		
		/*Navigate through entire List only once to get
		 * a smaller list if which hotels the items are in
		 * and their prices
		 */
		try {
			br = new BufferedReader(new FileReader(inputFile));
			while ((rowItems = br.readLine()) != null) {
				String[] Items = rowItems.split(",");
				for (int i = 1; i <= itemNum; i++) {
					if (Items.length == 3) {
						if (args[i].trim().equals(Items[2].trim())){
							if(!hotelHits.containsKey(Items[0])){
								hotelHits.put(Items[0], Items[2]+':'+Items[1]);
							} else {
								hotelHits.put(Items[0], hotelHits.get(Items[0])+","+Items[2]+':'+Items[1]);
							}
						}
					}
					else {
						boolean itemPresent = false;
						String mealConcat = "";
						for(int j=2; j<Items.length; j++){
							mealConcat = mealConcat + Items[j] +";";
							if (args[i].trim().equals(Items[j].trim())){
								itemPresent = true;
							}
							
						}
						if(itemPresent){
							if(!hotelHits.containsKey(Items[0])){
								hotelHits.put(Items[0], mealConcat.substring(1,mealConcat.length()-1)+':'+Items[1]);
							} else {
								hotelHits.put(Items[0], hotelHits.get(Items[0])+","+mealConcat.substring(1,mealConcat.length()-1)+':'+Items[1]);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("please enter valid input arguments");
		}
		
		/* 
		 * Getting the final list of hotels and total cost of order
		 * and sorting on price so that we can rank hotels
		 */
		TreeMap<Double, String> finalList = new TreeMap<Double, String>();
		for(Map.Entry<String, String> entry : hotelHits.entrySet()){
			if(getTotalCost(entry.getValue(), itemNum)!=-9999){
				finalList.put(getTotalCost(entry.getValue(), itemNum), entry.getKey());
			}
			
		}
		
		// Getting the top hit
		int rank = 1;
		if(finalList.size()==0)
			System.out.println("nil");
		else{
			for(Map.Entry<Double, String> entry : finalList.entrySet()){
				if(rank==1)
					System.out.println(entry.getValue()+", "+entry.getKey());
				rank++;
			}
		};
		
}
	/*
	 * Function for calculating the optimal cost of an order
	 * Handles items that are present individually and as meal contents
	 */
	public static double getTotalCost(String itemList, int itemNum){
		String[] items = itemList.split(",");
		if(items.length<itemNum){
			return -9999;
		} else {
		TreeMap<String, Double> optItems = new TreeMap<String, Double>();
		Map<String, Double> mealItems = new HashMap<String, Double>();
		for (String item : items) {
			String[] itemDetails = item.split(":");
			if(itemDetails[0].split(";").length==1){
				if(!optItems.containsKey(itemDetails[0].trim().toLowerCase())){
					optItems.put(itemDetails[0].trim().toLowerCase(), Double.parseDouble(itemDetails[1]));
				} else {
					if(optItems.get(itemDetails[0].trim().toLowerCase())>Double.parseDouble(itemDetails[1])){
						optItems.put(itemDetails[0].trim().toLowerCase(), Double.parseDouble(itemDetails[1]));
					}
				}
			} else {
				String[] itemsInMeal = itemDetails[0].split(";");
				for(String itemInMeal : itemsInMeal){
					if(!mealItems.containsKey(itemInMeal.trim().toLowerCase())){
						mealItems.put(itemInMeal.trim().toLowerCase(), Double.parseDouble(itemDetails[1]));
					} else {
						if(mealItems.get(itemInMeal.trim().toLowerCase())>Double.parseDouble(itemDetails[1])){
							mealItems.put(itemInMeal.trim().toLowerCase(), Double.parseDouble(itemDetails[1]));
						}
					}
				}
				
			}
			
		}
		//System.out.println(optItems.size());
		double sum = 0;
		boolean mealCounted = false;
		for(Map.Entry<String, Double> entry : optItems.entrySet()){
			if(mealItems.containsKey(entry.getKey())){
				if(entry.getValue()>mealItems.get(entry.getKey())){
						sum = sum - entry.getValue() + mealItems.get(entry.getKey());
					}
			} else 
				sum = sum + entry.getValue();
		}
		return sum;
		}
	}
}
